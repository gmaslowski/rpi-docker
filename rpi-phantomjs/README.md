# Phantomjs for raspberrypi

This repo will use the great work of this [githup repository][phantomjs] and wrap it in a Docker container.

Example Usage:

    $ # will print help
    $ docker run --rm -v $(pwd):/data schoeffm/rpi-phantomjs 

    $ # ... do the same with the contained wrapper-bash script
    $ ./phantomjs

    $ # will execute the rasterize.js example (located in your current directory)
    $ docker run --rm -v $(pwd):/data schoeffm/rpi-phantomjs /data/rasterize.js https://www.spiegel.de /data/spon.jpg "1200px*800px"

    $ # ... same as above but this one uses the included rasterize.js script
    $ docker run --rm -v $(pwd):/data schoeffm/rpi-phantomjs examples/rasterize.js https://www.spiegel.de /data/spon.jpg "1200px*800px"

[phantomjs]:https://github.com/piksel/phantomjs-raspberrypi
